import 'package:flutter/material.dart';
import 'dart:async';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'dart:io';
import 'package:transparent_image/transparent_image.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Asset> images = List<Asset>();
  String _error = '';

  @override
  void initState() {
    super.initState();
  }

  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return GestureDetector(
            child: AssetThumb(
              asset: asset,
              width: 300,
              height: 300,
            ),
          onTap: () {
              print('ontap called at $index');
          },
        );
      }),
    );


  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 100,
        enableCamera: true,
        selectedAssets: images,
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: const Text('Image picker'),
        ),
        floatingActionButton: FloatingActionButton.extended(
          label: Text('Add'),
          icon: Icon(Icons.add),
          backgroundColor: Colors.blue,

          onPressed: loadAssets,

        ),

        body: Column(
          children: <Widget>[
            Expanded(
              child: buildGridView(),
            )
          ],
        ),
      ),
    );
  }

}
